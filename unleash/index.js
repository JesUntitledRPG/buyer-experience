import { UnleashClient } from 'unleash-proxy-client';

// See all options in separate section.
const unleash = new UnleashClient({
  url: 'http://localhost:3000/proxy',
  clientKey: 'client-proxy-secret',
  appName: '[branch-name-here]',
});
unleash.start();

export { unleash };
