/**
 * Gets the last version number of the project and generates remotely the changelog on the main branch via GitLab API.
 *
 * Example: node ./gitlab-changelog.js
 */

const axios = require('axios');
const { version } = require('../package.json');

const branch = 'main';
const link = `https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/blob/${branch}/CHANGELOG.md`;
const endpoint =
  'https://gitlab.com/api/v4/projects/28847821/repository/changelog';

// Allow to access the environment variables
require('dotenv').config();

const { GITLAB_PROJECT_ACCESS_TOKEN } = process.env;

axios
  .post(
    endpoint,
    { version, branch },
    {
      headers: {
        'Content-Type': 'application/json',
        'PRIVATE-TOKEN': GITLAB_PROJECT_ACCESS_TOKEN,
      },
    },
  )
  .then((response) => {
    // eslint-disable-next-line promise/always-return
    if (response.status === 200) console.log(`Updated changelog: ${link}`);
  })
  .catch((err) => {
    console.error(err);
    throw new Error(err);
  });
